import {
  DELETE_SUCCESS, FETCH_COCKTAIL_SUCCESS, FETCH_COCKTAILS_SUCCESS,
  PUBLISH_COCKTAIL_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  cocktails: [],
  currentCocktail: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COCKTAILS_SUCCESS:
      return {...state, cocktails: action.cocktails};
    case FETCH_COCKTAIL_SUCCESS:
      return {...state, currentCocktail: action.cocktail};
    case PUBLISH_COCKTAIL_SUCCESS:
      return {...state, currentCocktail: action.cocktail};
    case DELETE_SUCCESS:
      return {...state, cocktails: action.cocktails, currentCocktail: {}};
    default:
      return state
  }
};

export default reducer;