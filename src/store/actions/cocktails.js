import axios from '../../axios-api';

import {
  CREATE_COCKTAIL_SUCCESS, DELETE_SUCCESS, FETCH_COCKTAIL_SUCCESS, FETCH_COCKTAILS_SUCCESS,
  PUBLISH_COCKTAIL_SUCCESS
} from "./actionTypes";
import {NotificationManager} from "react-notifications";
import {push} from "react-router-redux";

// post cocktail
export const createCocktail = cocktailData => async dispatch => {
  try {
    const response = await axios.post('/cocktails', cocktailData);
    dispatch(createCocktailSuccess(response.data.cocktail));
    dispatch(push('/'));
    NotificationManager.success('Success', response.data.message);
  } catch (err) {

  }
};

const createCocktailSuccess = cocktail => ({
  type: CREATE_COCKTAIL_SUCCESS, cocktail
});



// fetch all cocktails
export const fetchCocktails = () => async dispatch => {
  try {
    const response = await axios.get('/cocktails');
    dispatch(fetchCocktailsSuccess(response.data.cocktails));
  } catch (err) {

  }
};

const fetchCocktailsSuccess = cocktails => ({
  type: FETCH_COCKTAILS_SUCCESS, cocktails
});

// fetch one cocktail

export const fetchCocktail = cocktailId => async dispatch => {
  try {
    const response = await axios.get(`/cocktails/${cocktailId}`);
    dispatch(fetchCocktailSuccess(response.data));
  } catch (err) {

  }
};

const fetchCocktailSuccess = cocktail => ({
  type: FETCH_COCKTAIL_SUCCESS, cocktail
});

// send rating

export const sendRating = (ratingData, cocktailId) => async dispatch => {
  try {
    const res = await axios.post(`/cocktails/${cocktailId}`, {ratingData});
    dispatch(fetchCocktailSuccess(res.data));
  } catch (err) {

  }
};

// publish cocktail

export const publishCocktail = cocktailId => async dispatch => {
  try {
    const res = await axios.post(`/cocktails/publish/${cocktailId}`);
    dispatch(publishCocktailSuccess(res.data));
  } catch (err) {

  }
};

const publishCocktailSuccess = cocktail => ({
  type: PUBLISH_COCKTAIL_SUCCESS, cocktail
});

// delete

export const deleteCocktail = cocktailId => async dispatch => {
  try {
    const response = await axios.delete(`/cocktails/${cocktailId}`);
    dispatch(deleteCocktailsSuccess(response.data.cocktails));
    dispatch(push('/'));
    NotificationManager.success('Success', response.data.message);
  } catch (err) {

  }
};

const deleteCocktailsSuccess = cocktails => ({
  type: DELETE_SUCCESS, cocktails
});