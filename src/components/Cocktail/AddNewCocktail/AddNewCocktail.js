import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import CocktailForm from "../CocktailForm/CocktailForm";
import {createCocktail} from "../../../store/actions/cocktails";

class AddNewCocktail extends Component {

  createCocktailHandler = cocktailData => {
    this.props.onCreateCocktail(cocktailData);
  };

  render() {
    return (
      <Fragment>
        <PageHeader>Add new cocktail</PageHeader>
        <CocktailForm
          onSubmit={this.createCocktailHandler}
        />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onCreateCocktail: cocktailData => dispatch(createCocktail(cocktailData))
});

export default connect(null, mapDispatchToProps)(AddNewCocktail);