import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";

import FormElement from "../../UI/FormElement/FormElement";
import IngredientsForm from "../IngredientForm/IngredientsForm";

class CocktailForm extends Component {

  state = {
    title: '',
    image: '',
    recipe: '',
    ingredients: [{id: 1, name: '', amount: ''}],
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  ingredientChangeHandler = (event, index) => {
    const ingredients = [...this.state.ingredients];
    ingredients[index][event.target.name] = event.target.value;

    this.setState({ingredients});
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  sendCocktailHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      if (key !== 'ingredients') {
        formData.append(key, this.state[key])
      } else {
        formData.append(key, JSON.stringify(this.state[key]));
      }
    });

    this.props.onSubmit(formData);
  };

  addIngredientsForm = () => {
    const ingredients = [...this.state.ingredients];
    ingredients.push({name: '', amount: ''});

    this.setState({ingredients})
  };

  removeIngredientsForm = index => {
    const ingredients = [...this.state.ingredients];

    ingredients.splice(index, 1);
    this.setState({ingredients});
  };

  render() {
    return(
      <Fragment>
        <Form horizontal onSubmit={this.sendCocktailHandler}>
          <FormElement required
                       propertyName='title'
                       title='Title'
                       placeholder='Enter title'
                       type='text'
                       autoComplete='current-title'
                       value={this.state.title}
                       changeHandler={this.inputChangeHandler}
          />

          <FormGroup>

            {this.state.ingredients.length > 0 &&
              this.state.ingredients.map(ingredient => {
                const index = this.state.ingredients.indexOf(ingredient);

                return (
                  <IngredientsForm
                      key={index}
                      id={ingredient._id}
                      changeHandler={(event) => this.ingredientChangeHandler(event, index)}
                      valueForName={this.state.ingredients[index].name}
                      valueForAmount={this.state.ingredients[index].amount}
                      remove={() => this.removeIngredientsForm(index)}
                  />
                )
              })
            }
            <Col md={12}>
              <Button onClick={this.addIngredientsForm}>
                Add ingredient
              </Button>
            </Col>
          </FormGroup>


          <FormElement required
                       propertyName='recipe'
                       title='Recipe'
                       placeholder='Enter recipe'
                       type='textarea'
                       value={this.state.recipe}
                       changeHandler={this.inputChangeHandler}
          />

          <FormElement required
                       propertyName='image'
                       title='Image'
                       type='file'
                       changeHandler={this.fileChangeHandler}
          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">
                Create cocktail
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

export default CocktailForm;