import React from 'react';
import {Col, Image, ListGroup, ListGroupItem, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";

const Cocktail = props => (
  <Col md={4}>
    <Panel>
      <Panel.Body>
        <p><Link to={props.path}>{props.title} </Link>
          <b>by: </b>{props.username}
        </p>
        <Image thumbnail src={props.image}/>
        <p><b>Ingredients: </b></p>
        <ListGroup>
          {props.ingredients.map((ingredient, index) => (
            <ListGroupItem key={index}>
              {ingredient.name} - {ingredient.amount}
            </ListGroupItem>
          ))}
        </ListGroup>
        <p>Recipe: {props.recipe}</p>
      </Panel.Body>
      {props.user && props.user.role === 'admin' &&
        <Panel.Footer>
          <b>
            {props.published
                ? 'Published'
                : 'Not published'
            }
          </b>
        </Panel.Footer>
      }
    </Panel>
  </Col>
);

export default Cocktail;