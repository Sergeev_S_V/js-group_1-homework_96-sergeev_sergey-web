import React, {Fragment} from 'react'
import {Button, Col, ControlLabel, FormControl, FormGroup, Row} from "react-bootstrap";

const IngredientsForm = props => (
  <Fragment>
    <Row>
      <Col componentClass={ControlLabel} md={1}>
        Ingredients
      </Col>
      <Col md={4}>
        <FormGroup controlId="ingredientName">
          <Col md={12}>
            <FormControl
              required
              type="text"
              name="name"
              value={props.value}
              placeholder="Ingredient name"
              onChange={props.changeHandler}
            />
          </Col>
        </FormGroup>
      </Col>
      <Col md={4}>
        <FormGroup controlId="ingredientsAmount">
          <Col md={12}>
            <FormControl
              required
              type="text"
              name="amount"
              value={props.value}
              placeholder="Ingredients amount"
              onChange={props.changeHandler}
            />
          </Col>
        </FormGroup>
      </Col>
      {!props.id &&
        <Col md={2}>
          <Button onClick={props.remove}>Delete</Button>
        </Col>
      }
    </Row>
  </Fragment>
);

export default IngredientsForm;