import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddNewCocktail from "./components/Cocktail/AddNewCocktail/AddNewCocktail";
import CocktailsList from "./containers/CocktailsList/CocktailsList";
import CocktailPage from "./containers/CocktailPage/CocktailPage";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login" />
);

const Routes = ({user}) => (
  <Switch>
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
    <Route path="/cocktails/:id" exact
           render={(props) => <CocktailPage {...props} user={user}/>}
    />
    <ProtectedRoute
      isAllowed={user}
      path="/add_new_cocktail"
      exact
      component={AddNewCocktail}
    />
    <Route path="/" exact component={CocktailsList}/>
    <Route path="/cocktails" exact component={CocktailsList}/>
  </Switch>
);

export default Routes;