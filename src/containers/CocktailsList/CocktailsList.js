import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchCocktails} from "../../store/actions/cocktails";
import Cocktail from "../../components/Cocktail/Cocktail";
import {Row} from "react-bootstrap";
import config from "../../config";

const imagePath = config.apiUrl + '/uploads/';

class CocktailsList extends Component {

  componentDidMount() {
    this.props.onFetchCocktails();
  };

  renderCocktails = cocktails => {
    return (
      cocktails.map(cocktail => {
        return (
          <Cocktail
            key={cocktail._id}
            title={cocktail.title}
            recipe={cocktail.recipe}
            image={imagePath + cocktail.image}
            ingredients={cocktail.ingredients}
            ratings={cocktail.ratings}
            username={cocktail.userId.username}
            path={`/cocktails/${cocktail._id}`}
            published={cocktail.published}
            user={this.props.user}
          />
        )
      })
    );
  };

  render() {
    return(
      <Fragment>
        <Row>
          {this.props.cocktails.length > 0 &&
            this.renderCocktails(this.props.cocktails)
          }
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cocktails: state.cocktails.cocktails,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchCocktails: () => dispatch(fetchCocktails())
});

export default connect(mapStateToProps, mapDispatchToProps)(CocktailsList);