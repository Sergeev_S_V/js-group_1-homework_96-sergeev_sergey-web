import React, {Component, Fragment} from 'react'
import {connect} from "react-redux";
import Rating from 'react-rating';

import {deleteCocktail, fetchCocktail, publishCocktail, sendRating} from "../../store/actions/cocktails";
import config from "../../config";
import {Button, Col, Image, ListGroup, ListGroupItem, Panel} from "react-bootstrap";

const pathImage = config.apiUrl + '/uploads/';

class CocktailPage extends Component {

  state = {
    rating: 0
  };

  componentDidUpdate(prevProps) {
    if (this.props.cocktail !== prevProps.cocktail) {
      if (this.props.cocktail && this.props.cocktail.ratings.length > 0) {
        this.setState({rating: this.props.average});
      }
    }
  };

  componentDidMount() {
    this.props.onFetchCocktail(this.props.match.params.id);
  };

  setRating = value => {
    this.setState({rating: value});

    const ratingData = {
      userId: this.props.user._id,
      rating: value
    };

    const cocktailId = this.props.cocktail._id;
    this.props.onSendRating(ratingData, cocktailId);
  };

  publishCocktailHandler = () => {
    const cocktailId = this.props.cocktail._id;
    this.props.onPublishCocktail(cocktailId);
  };

  render() {
    return(
      <Fragment>
        {this.props.cocktail &&
          <Col md={4}>
            <Panel>
              <Panel.Heading>
                <span>{this.props.cocktail.title}</span>
                <span> <b>Rating:</b> {this.props.average} </span>
                {/*<span> {this.props.cocktail.votes}<b> votes</b></span>*/}
              </Panel.Heading>
              <Panel.Body>
                {this.props.cocktail.image &&
                  <Image thumbnail src={pathImage + this.props.cocktail.image}/>
                }
                <p>Ingredients: </p>
                <ListGroup>
                  {this.props.cocktail.ingredients.map((ingredient, index) => (
                    <ListGroupItem key={index}>
                      {ingredient.name} - {ingredient.amount}
                    </ListGroupItem>
                  ))}
                </ListGroup>
                <p>Recipe: {this.props.cocktail.recipe}</p>
              </Panel.Body>
              <Panel.Footer>
                <b>Rate: </b>
                <Rating
                  {...this.props}
                  initialRating={this.state.rating}
                  onChange={(value) => {this.setRating(value)}}
                />

                {this.props.user && this.props.user.role === 'admin' &&
                  <div>
                    <Button onClick={() => this.props.onDeleteCocktail(this.props.cocktail._id)}>
                      Delete
                    </Button>
                    <Button
                      disabled={this.props.cocktail.published}
                      onClick={this.publishCocktailHandler}
                    >
                      Publish
                    </Button>
                    <span>
                      {
                        this.props.cocktail.published
                          ? 'Published'
                          : 'Not published'
                      }
                    </span>
                  </div>
                }

              </Panel.Footer>
            </Panel>
          </Col>
        }
      </Fragment>
    );
  }
}


const mapStateToProps = state => ({
  cocktail: state.cocktails.currentCocktail.cocktail,
  average: state.cocktails.currentCocktail.average
});

const mapDispatchToProps = dispatch => ({
  onFetchCocktail: cocktailId => dispatch(fetchCocktail(cocktailId)),
  onSendRating: (ratingData, cocktailId) => dispatch(sendRating(ratingData, cocktailId)),
  onPublishCocktail: cocktailId => dispatch(publishCocktail(cocktailId)),
  onDeleteCocktail: cocktailId => dispatch(deleteCocktail(cocktailId))
});

export default connect(mapStateToProps, mapDispatchToProps)(CocktailPage);